#!/bin/bash
cd "$(cat /tmp/compath1)"

#changes size of terminal window
#tip from here http://apple.stackexchange.com/questions/33736/can-a-terminal-window-be-resized-with-a-terminal-command
#Will move terminal window to the left corner

printf '\e[8;13;66t'
printf '\e[3;410;100t'

open -a Terminal

bold="$(tput bold)"
normal="$(tput sgr0)"
red="$(tput setaf 1)"
reset="$(tput sgr0)"
green="$(tput setaf 2)"
underline="$(tput smul)"
standout="$(tput smso)"
normal="$(tput sgr0)"
black="$(tput setaf 0)"
red="$(tput setaf 1)"
green="$(tput setaf 2)"
yellow="$(tput setaf 3)"
blue="$(tput setaf 4)"
magenta="$(tput setaf 5)"
cyan="$(tput setaf 6)"
white="$(tput setaf 7)"

#get rid of not needed file
    if ls .DS_store >/dev/null 2>&1;
    then 
    rm .DS_store
    fi

clear
#check for dependencies:
if ! [ -f "`which hg`" ]
then 
clear
echo $(tput bold)"
Checking for mercurial, please wait..."
sleep 2
read -p $(tput bold)"
mercurial(hg) is not installed would you like to install it?
(this action might install homebrew as well)$(tput setaf 1)

Y/N?"$(tput sgr0) -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
clear
echo "Follow instructions in terminal window"
sleep 2
[ ! -f "`which brew`" ] && /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install mercurial
fi
fi
#Did we run hgrc changes?:
    dir=$PWD

#repository name. Expecting this to cause problems in different repos. Needs testing
repname="$(grep 'bitbucket.org' "$dir"/.hg/hgrc | cut -d "." -f2 | cut -d "/" -f3 | head -1)"

clear
while :
do 
    cat<<EOF
    $(tput sgr0)========================	   
    ${bold}$(tput setaf 1)hg-git automation script$(tput sgr0)
    ------------------------
 
    $(tput bold)(c)  hg commit$(tput sgr0)
    $(tput bold)(p)  git, pull, add files, commit, push$(tput sgr0)
    $(tput bold)(s)  git pull from $repname repo$(tput sgr0)
    $(tput bold)$(tput setaf 1)(m)  main$(tput sgr0)
    $(tput bold)$(tput setaf 1)(q)  exit from this menu$(tput sgr0)

Please enter your selection number below:
EOF
    read -n2
    case "$REPLY" in

   "c")  
  cd "$dir"/
clear
#commit
   cd "$dir"/
clear
echo ""
cat<<EOF
$(tput bold)$(tput setaf 1)(m)  main$(tput sgr0)
$(tput bold)$(tput setaf 1)(q)  exit from this menu$(tput sgr0)
EOF
echo ""
echo ""
echo ""
   echo $(tput bold)"Write your username:$(tput sgr0) then press enter"
   read user
if [ "$user" = m ];
then
clear
cd "$(cat /tmp/compath1)"
. "$(cat /tmp/compath2)"/main.command
fi
if [ "$user" = q ];
then
osascript -e 'tell application "Terminal" to close first window' & exit
fi
clear
echo ""
cat<<EOF
$(tput bold)$(tput setaf 1)(m)  main$(tput sgr0)
$(tput bold)$(tput setaf 1)(q)  exit from this menu$(tput sgr0)
EOF
echo ""
echo ""
echo ""
   echo $(tput bold)"Write a commit message$(tput sgr0) then press enter"
   read commit
if [ "$commit" = m ];
then
clear
cd "$(cat /tmp/compath1)"
. "$(cat /tmp/compath2)"/main.command
fi
if [ "$commit" = q ];
then
osascript -e 'tell application "Terminal" to close first window' & exit
fi
#get rid of not needed file
    if ls .DS_store >/dev/null 2>&1;
    then 
    rm .DS_store
    fi
    hg update
    hg addremove
    hg --config ui.username="$(echo $user)" commit -m "$(echo $commit)"
;;


  "p")  
  cd "$dir"/
clear
#commit
clear
echo ""
cat<<EOF
$(tput bold)$(tput setaf 1)(m)  main$(tput sgr0)
$(tput bold)$(tput setaf 1)(q)  exit from this menu$(tput sgr0)
EOF
echo ""
echo ""
echo ""
   echo $(tput bold)"Write a commit message$(tput sgr0) then press enter"
   read commit
if [ "$commit" = m ];
then
clear
cd "$(cat /tmp/compath1)"
. "$(cat /tmp/compath2)"/main.command
fi
if [ "$commit" = q ];
then
osascript -e 'tell application "Terminal" to close first window' & exit
fi
#get rid of not needed file
    if ls .DS_store >/dev/null 2>&1;
    then 
    rm .DS_store
    fi
    git pull
    git add .
    git commit -m "$(echo $commit)"
    git push
;;

   "s")  
#pull from source
   cd "$dir"/
clear
#get rid of not needed file
    if ls .DS_store >/dev/null 2>&1;
    then 
    rm .DS_store
    fi
    git pull 
;;

    "m") 
cd "$(cat /tmp/compath1)"
. "$(cat /tmp/compath2)"/main.command
    ;;

   "q")  
osascript -e 'tell application "Terminal" to close first window' & exit
;;

    "Q")  echo "case sensitive!!"   ;;
     * )  echo "invalid option"     ;;
    esac
    sleep 0.5
done






