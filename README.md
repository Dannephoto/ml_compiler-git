# Compiler #

Automation tool which simplifies the creation of a magic-lantern environment through some user friendly bash menus.
Most stuff is built around this(thanks dfort):
https://www.magiclantern.fm/forum/index.php?topic=16012.0
Forum_thread:
https://www.magiclantern.fm/forum/index.php?topic=21882.msg199370#msg199370

**Main menu**

![Screen Shot 2018-04-04 at 11_500px.tif](https://s18.postimg.cc/mqowmxbe1/Screen_Shot_2018-04-20_at_10_500px.png)


## HOWTO ##
1. Double click Compiler.app and follow instructions(check below how to bypass gatekeeper)
2. To change or add a repository just drag your magic-lantern repository onto Compiler.app


**regarding gatekeeper**
To supress gatekeeper hold ctrl button down(macOS Sierrra) while opening the application the first time. You can also change permissions from within privacy/security settings.


**Thanks to:** ML team

**License: GPL**

